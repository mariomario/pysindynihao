
import json
import matplotlib.pyplot as plt
import numpy as np



f = open('data_tgreen.json', 'r')
data_all = json.load(f)
f.close()



##### plot quantities versus time

all_galaxies = data_all[0].keys()
galname = list(all_galaxies)[15]
print(galname)

N = data_all[0][galname]['n_steps']
time = [data_all[i][galname]['time'] for i in range(N)]
mstar = [data_all[i][galname]['mstar'] for i in range(N)]
mbh_c = [data_all[i][galname]['mbh_c'] for i in range(N)]
color = [data_all[i][galname]['u-r'] for i in range(N)]

plt.clf()
plt.plot(time,color)
plt.savefig('color.png')

#####




parameters = ['time_AGN', 't_blue','t_red','tg_simp','t_blue_true','t_red_true','tg_true','t_blue_sfonly','t_red_sfonly','tg_sfonly','age_sfonly','d_mstar','age_tb','met_tb','age_tr','met_tr','mstar_tb','mstar_tr','mstar_z0','kappa_tb','kappa_tr','kappa_z0']

all_galaxies = data_all[0].keys()

data_coll = dict()

for key in parameters:
    data_coll[key] = []
    for galname in all_galaxies:
        data_coll[key] += data_all[0][galname][key]




##### plot true green valley time versus mean stellar age at t_blue

plt.clf()
plt.scatter(data_coll['age_tb'],data_coll['tg_true'])
plt.savefig('tg_age.png')

#####
